#!/usr/bin/env bash

if [[ -d "$0.d" ]]; then
	run-parts "${@/#/--arg=}" -- "$0.d"
fi
file="$GIT_DIR/hooks/${0##*/}"
if [[ -x "$file" && -f "$file" ]]; then
	"$file" "$@"
fi
